import os

BASE_DIR = os.path.dirname(__file__)
TELEGRAM_TOKEN = os.getenv("TELEGRAM_TOKEN")
DB_PATH = os.path.join(BASE_DIR, os.getenv("DB_NAME", "sql_app.db"))
