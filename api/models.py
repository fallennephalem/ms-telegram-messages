from sqlalchemy import Column, Integer, String, Text, ForeignKey
from sqlalchemy.orm import relationship

from api.database import Base


class Project(Base):
    __tablename__ = "projects"

    id = Column(Integer, primary_key=True, index=True)
    project_id = Column(Integer, unique=True, index=True)
    chat_id = Column(Integer, unique=True, index=True)

    messages = relationship("Message", back_populates="project")


class Message(Base):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True, index=True)
    message_id = Column(Integer)
    message = Column(Text)
    author_name = Column(String)
    author_username = Column(String)
    created = Column(Integer)
    project_id = Column(Integer, ForeignKey("projects.id"))

    project = relationship("Project", back_populates="messages")
